#include "f4t.h"

#define PI (3.14159f)
#define TAU (2.0f*PI)

void f4t_dft(float complex *in, float complex *out, size_t n) {
    for (size_t freq_i = 0; freq_i < n; freq_i += 1) {
        out[freq_i] = 0;

        for (size_t i = 0; i < n; i += 1) {
            float t = (float)i / (float)n;
            out[freq_i] += in[i] * cexpf(-I * TAU * (float)freq_i * t);
        }
    }
}

void fft(float complex *restrict buf, float complex *restrict out, int n, int step)
{
    if (step >= n) {
        return;
    }

    fft(out, buf, n, step*2);
    fft(out+step, buf+step, n, step*2);

    float n_inv = 1.0f / (float)n;
    float complex exponent = -I * PI * n_inv;
    for (int i = 0; i < n; i += 2 * step) {
        float complex t = out[step+i] * cexp(exponent*i);

        buf[i/2]       = out[i] + t;
        buf[(n + i)/2] = out[i] - t;
    }
}

void f4t_fft(float complex *in, float complex *out, size_t n) {
    float complex in_cmplx[n];
    for (size_t i = 0; i < n; i++) in_cmplx[i] = in[i];

    fft(out, in_cmplx, n, 1);
}

