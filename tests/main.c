#include <f4t.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int main(void) {

    printf("== testing F(x) = sin(omega) + sin(omega*3) ==\n");
    
    size_t sin_len = 1<<3;
    float complex *in_wave = malloc(sizeof(float complex) * sin_len);

    for (size_t i = 0; i < sin_len; i += 1) {
        in_wave[i] =  sinf(2 * 3.14159 * i / sin_len);
        in_wave[i] += sinf(2 * 3.14159 * i / sin_len * 3);
        in_wave[i] = i < 4 ? 1 : 0;
    }

    printf("wave: ");
    for (size_t i = 0; i < sin_len; i += 1) {
        printf("%f ", creal(in_wave[i]));
    }
    printf("\n");

    f4t_fft(in_wave, in_wave, sin_len);

    char fft_res[128];
    char *fft_res_ptr = fft_res;
    for (size_t i = 0; i < sin_len; i += 1) {
        size_t n_printed = sprintf(fft_res_ptr, "(%.1f, %.1f) ", creal(in_wave[i]), cimag(in_wave[i]));
        fft_res_ptr += n_printed;
    }

    // validate
    const char *expected_sin_freqs = "(4.0, 0.0) (1.0, -2.4) (0.0, 0.0) (1.0, -0.4) (0.0, 0.0) (1.0, 0.4) (0.0, 0.0) (1.0, 2.4) ";
    if (strncmp(fft_res, expected_sin_freqs, 128) != 0) {
        printf("== f4t_fft returned invalid results ==\ngot:      %s\nexpected: %s\n", fft_res, expected_sin_freqs);
        return 1;
    }

    printf("frequencies: %s\n", fft_res);

    free(in_wave);

    printf("== finished ==\n");


    printf("== testing 65536 (2<<16) samples ==\n");
    size_t large_len = 2<<16;
    in_wave = malloc(sizeof(float complex) * large_len);
    
    // time the function
    clock_t start = clock();
    f4t_fft(in_wave, in_wave, large_len);
    clock_t end = clock();

    int ms = (end - start) * 1000 / CLOCKS_PER_SEC;

    printf("== finished (%dms) ==\n", ms);

    return 0;
}

