CFILES = $(wildcard src/*.c tests/*.c)
OFILES = $(patsubst %.c, build/%.o, $(CFILES))

CC = gcc
FLAGS = -g -std=c99 -Wall -Wextra -pedantic -Werror
INCLUDES = -Iinclude

DEMO = build/f3t

.Phony: all
all: $(DEMO)

run: $(DEMO)
	./$(DEMO)

$(DEMO): $(OFILES)
	$(CC) $(FLAGS) $^ -o $@

build/%.o: %.c
	mkdir -p $(@D)
	$(CC) -c $^ $(FLAGS) $(INCLUDES) -Isrc -o $@

.Phony: clean
clean:
	rm -r build

