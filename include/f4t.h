#ifndef F3T_H
#define F3T_H

#include <complex.h>
#include <stddef.h>


// applies the discrete fourier transform. d3t_fft should be used instead. this
// is mainly for completeness.
//
// out must be preallocated
void f4t_dft(float complex *in, float complex *out, size_t n);

// applies the fast fourier transform.
//
// out must be preallocated
void f4t_fft(float complex *in, float complex *out, size_t n);

// applies the inverse fourier transform.
//
// out must be preallocated
void f4t_ift(float complex *in, float complex *out, size_t n);

#endif
